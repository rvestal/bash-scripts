# bash-scripts

My repository of bash scripts I've used in my career. Most are going to be CentOS/RHEL based. Feel free to use them at no charge, but please give me credit for the work.

* clean-prep.sh - This script cleans a CentOS/RHEL system so it can be shutdown and used as a template. I use this to build VM templates.
* install-clamav-centos7.sh - Remotely connect using ssh and install clamav and update config files from preconfigured files.
* install-rpm.sh - Remotely connect using ssh and install an RPM using rpm command
* remove-rpm.sh - Remotely connect using ssh and remove an RPM using rpm command
* is-it-pingable.sh - Does a simple ping -c1 to verify if a system is pingable and outputs to text file
* addusers.sh - Add users from text file and set the password to a new temp password and set aging to require password change at next login
* guacamole-install-centos7.sh - Install guacamole 1.2.0 onto CentOS7. If there are issues after the install, check the SELINUX settings. Must be run as root.
* centos8-migrate-to-stream.sh - Migrate CentOS8 to CentOS Stream. This script sets settings necessary are runs the conversion.
* linux-updates - Update remote linux systems using yum. See script for requirements