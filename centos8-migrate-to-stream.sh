#!/bin/bash

###################################################################
##
## CentOS8 with FIPS enabled tends to crash with a core dump when
## migrating to CentOS Stream. This script will disable FIPS 
## and then upgrade the system. Once complete it will then
## re-enable FIPS
##
##################################################################

## Test if running as root


## Test if FIPS is enabled


## Set counter 
 

## Disable FIPS
fips-mode-setup --disable

## Reboot system
echo " "
echo " "
echo "FIPS has been disabled. Rerun this script to continue"
reboot
break
# Sync with Stream
#dnf distro-sync -y

## Run update
#dnf update -y

## Re-enable FIPS
#fips-mode-setup --enable

## Reboot system
#reboot


