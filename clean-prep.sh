#!/bin/bash

#################################################################
##
##  Clean a CentOS/RHEL system and get it ready to create as 
##  a template.
##
#################################################################

YELLOW='\033[1;33m'
NOCOLOR='\033[0m'

echo -e "${YELLOW}Removing Ansible${NOCOLOR}"
yum remove ansbile

echo ""
echo -e "${YELLOW}stopping rsyslog and auditd services ${NOCOLOR}"
/sbin/service rsyslog stop
/sbin/service auditd stop

echo ""
echo -e "${YELLOW}Removing Old Kernels${NOCOLOR}"
dnf remove --oldinstallonly --setopt installonly_limit=2 kernel -y

echo ""
echo -e "${YELLOW}Cleaning Up YUM ${NOCOLOR}"
yum clean all
rm -rf /var/cache/yum

echo ""
echo -e "${YELLOW}Cleaning up logs ${NOCOLOR}"
logrotate -f /etc/logrotate.conf
rm -f /var/log/*-???????? /var/log/*.gz
rm -f /var/log/dmesg.old
rm -rf /var/log/anaconda
cat /dev/null > /var/log/audit/audit.log
cat /dev/null > /var/log/wtmp
cat /dev/null > /var/log/lastlog
cat /dev/null > /var/log/grubby

echo ""
echo -e "${YELLOW}Cleaning up network UUIDs${NOCOLOR}"
rm -f /etc/udev/rules.d/70*

echo ""
echo -e "${YELLOW}Cleaning up tmp ${NOCOLOR}"
rm -rf /tmp/*
rm -rf /var/tmp/*

echo ""
echo -e "${YELLOW}Removing system ssh keys${NOCOLOR}"
rm -f /etc/ssh/*key*

echo ""
echo -e "${YELLOW}Removing Ansible user settings${NOCOLOR}"
rm -rf /home/wbxbuilds/.ansible
rm -rf /home/wbxbuilds/.ssh
rm -rf /etc/ansible

echo ""
echo -e "${YELLOW}Removing root settings${NOCOLOR}"
rm -rf /root/.ansible
rm -rf /root/.ssh/
rm -f /root/anaconda-ks.cfg

echo ""
echo -e "${YELLOW}Done!${NOCOLOR}"
