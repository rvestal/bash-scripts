#!/bin/sh

## Add new users from newusers.txt file,
## set the temp password to "NewP@ss!",
## set aging to require password reset
## at next login
## Author - Roy Vestal

for i in `cat ~:/newusers.txt`
do
echo $i
adduser -m $i
done

for i in `more foo`
do
echo NewP@ss! | passwd $i --stdin
chage -d 0 $i
done
