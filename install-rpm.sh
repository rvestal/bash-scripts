#!/bin/bash

#################################################################
##
## Remotely connect using ssh and install an RPM using rpm command
##
#################################################################


RPMNAME=<rpmname>
#SCRIPT="ls /tmp"
SCRIPT1="sudo rpm -e $RPMNAME "
SCRIPT2="sudo rpm -qa | grep $RPMNAME"


PASSWORD="<password>

for HOST in `cat hosts.txt` ; 
	do 
		echo " "
		echo " "
		echo "Copying $RPMNAME to $HOST:/tmp"

		sshpass -p $PASSWORD scp /tmp/$RPMNAME $HOST:/tmp
		
		echo " "
		echo " "
		echo "Installing $RPMNAME on $HOST"
		
		sshpass -p $PASSWORD ssh $HOST $SCRIPT1

#		sshpass -p $PASSWORD ssh -o StrictHostKeyChecking=no $HOST $SCRIPT
		sshpass -p $PASSWORD ssh $HOST $SCRIPT2

	done
