# This script will install guacamole 1.2.0 onto CentOS7.
# If there are issues after the install, check the SELINUX settings.
# This script must be run as root.
# Guacamole server must be built from source. The client will be downloaded.
# Guacamole website: https://guacamole.apache.org
# Guacamole Manual: https://guacamole.apache.org/doc/gug/

## verify system is up to date

echo "Updating server..."
echo " "
echo " "

sudo yum clean all
sudo yum update -y

# Install the Development Tools needed for Guacamole server build.

echo "Installing Development Tools..."
echo " "
echo " "

sudo yum groupinstall "Development Tools" -y

# Install the required libraries for Guacamole server build.

echo "Installing required libraries..."
echo " "
echo " "

sudo yum install -y wget cairo-devel libjpeg-turbo-devel libjpeg-devel libpng-devel libtool uuid-devel tomcat freerdp-devel pango-devel libssh2-devel libtelnet-devel libvncserver-devel libwebsockets-devel pulseaudio-libs-devel openssl-devel libvorbis-devel libwebp-devel freerdp telnet maven

sudo yum localinstall --nogpgcheck -y https://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm

sudo yum install -y ffmpeg-devel

# Download the Guacamole server source and build

echo "Download the Guacamole server source and build"
echo " "
echo " "

cd /tmp

wget https://downloads.apache.org/guacamole/1.2.0/source/guacamole-server-1.2.0.tar.gz

tar -xzf guacamole-server-1.2.0.tar.gz

cd /tmp/guacamole-server-1.2.0

./configure --with-init-dir=/etc/init.d

make

sudo make install

sudo ldconfig

# Download the Guacamole client and setup with Tomcat service
echo "Download the Guacamole client and setup with Tomcat service..."
echo " "
echo " "

wget https://downloads.apache.org/guacamole/1.2.0/binary/guacamole-1.2.0.war

sudo cp guacamole-1.2.0.war /var/lib/tomcat/webapps/guacamole.war

# Update firewall to allow connections
echo "Update firewall connections..."
echo " "
echo " "


firewall-cmd --permanent --zone=public --add-port=8080/tcp

firewall-cmd --permanent --zone=public --add-port=4822/tcp

sudo service firewalld restart

# Create /etc/guacamole/guacamole.properties
echo "Create /etc/guacamole/guacamole.properties..."
echo " "
echo " "

sudo mkdir guacamole

cd /etc/guacamole

echo "# Hostname and port of guacamole proxy" >> /etc/guacamole/guacamole.properties
echo "guacd-hostname: localhost" >> /etc/guacamole/guacamole.properties
echo "guacd-port:     4822" >> /etc/guacamole/guacamole.properties

# Restart all services
echo "Restart all services..."
echo " "
echo " "

sudo service tomcat restart

sudo service tomcat status

sudo service guacd restart

sudo service guacd status

echo "Guacamole install complete. If there are issues, check the SELinux settings"
echo "Enjoy!"
