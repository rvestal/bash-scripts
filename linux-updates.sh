#!/bin/bash

#################################################################
##
## Remotely connect using ssh and update using dnf
##
#################################################################


#RPMNAME=<rpmname>
#SCRIPT="ls /tmp"
SCRIPT1="sudo update"
SCRIPT2="sudo upgrade -y"


PASSWORD="<password>"

for HOST in `cat hosts.txt` ; 
	do 
		echo " "
		echo " "
		echo "Running DNF Update"

		sshpass -p $PASSWORD ssh $HOST $SCRIPT1
		
		echo " "
		echo " "
		echo "Running DNF Upgrade"
		

#		sshpass -p $PASSWORD ssh -o StrictHostKeyChecking=no $HOST $SCRIPT
		sshpass -p $PASSWORD ssh $HOST $SCRIPT2

	done
