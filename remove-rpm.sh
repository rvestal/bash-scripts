#!/bin/bash

#################################################################
##
## Remotely connect using ssh and remove an RPM using rpm command
##
#################################################################


RPMNAME=<rpmname>
#SCRIPT="ls /tmp"
SCRIPT1="sudo rpm -e $RPMNAME "
SCRIPT2="sudo rpm -qa | grep $RPMNAME 



PASSWORD="<password>"

for HOST in `cat hosts.txt` ; 
	do 
		echo " "
		echo " "
		echo " Removing $RPMNAME $HOST"
		sshpass -p $PASSWORD ssh $HOST $SCRIPT1
		sshpass -p $PASSWORD ssh $HOST $SCRIPT2

	done
