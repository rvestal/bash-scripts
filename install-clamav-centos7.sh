#!/bin/bash

########################################################################
##
## Remotely connect using ssh and install clamav and update config files
## from preconfigured files.
##
########################################################################

sudo yum clean all

sudo yum -y install clamav-server clamav-data clamav-update clamav-filesystem clamav clamav-scanner-systemd clamav-lib clamav-server-systemd

sudo mv /etc/clamd.d/scan.conf /etc/clamd.d/scan.conf.orig

sudo cp /tmp/clam/scan.conf /etc/clamd.d/scan.conf

sudo chown root.root /etc/clamd.d/scan.conf

sudo mv /etc/freshclam.conf /etc/freshclam.conf.orig

sudo cp /tmp/clam/freshclam.conf /etc/freshclam.conf

sudo mkdir /var/log/clamav

sudo chown clamupdate:virusgroup /var/log/clamav

sudo chmod 775 /var/log/clamav

sudo chown clamupdate:virusgroup /var/lib/clamav

sudo chmod 755 /var/lib/clamav

sudo chmod 660 /var/lib/clamav/*

sudo cp /tmp/clam/freshclam /etc/cron.daily/freshclam

sudo cp /tmp/clam/clamscan.cron /etc/cron.daily/clamscan.cron

sudo chmod a+x /etc/cron.daily/freshclam

sudo chmod a+x /etc/cron.daily/clamscan.cron

sudo freshclam

sudo /etc/cron.daily/clamscan.cron


